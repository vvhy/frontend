package com.example.bfu_helper_10;

import android.graphics.Bitmap;

import java.util.Date;

public class NewsEntry {
    public Date date;
    public Bitmap bitmap;
    public String synopsis;
    public String full_text;

    public NewsEntry(Date date, Bitmap bitmap, String synopsis, String full_text) {
        this.date = date;
        this.bitmap = bitmap;
        this.synopsis = synopsis;
        this.full_text = full_text;
    }
}
