package com.example.bfu_helper_10;

import android.app.Notification;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

public class CardAdapter extends RecyclerView.Adapter<CardAdapter.ViewHolder2> {

    private final List<Subject> subjects;

    public CardAdapter(List<Subject> subjects) {
        this.subjects = subjects;
    }

    @NonNull
    @Override
    public ViewHolder2 onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_card, parent, false);
        return new ViewHolder2(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder2 holder, int position) {
        Subject subject = subjects.get(position);
        holder.subjectImage.setImageResource(subject.getImage());
        holder.subjectName.setText(subject.getSubject());
        holder.time.setText(subject.getTime());
        holder.cardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(v.getContext(), subject.getSubject(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    public int getItemCount() {
        return subjects.size();
    }

    public static class ViewHolder2 extends RecyclerView.ViewHolder {
        final TextView time, subjectName;
        final ImageView subjectImage;
        final CardView cardView;

        public ViewHolder2(@NonNull View itemView) {
            super(itemView);
            time = (TextView) itemView.findViewById(R.id.time);
            subjectName = (TextView) itemView.findViewById(R.id.subjectName);
            subjectImage = (ImageView) itemView.findViewById(R.id.subjectImage);
            cardView = (CardView) itemView.findViewById(R.id.buttonSubject);
        }
    }
}
