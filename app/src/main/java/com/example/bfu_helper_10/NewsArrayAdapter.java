package com.example.bfu_helper_10;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

public class NewsArrayAdapter extends ArrayAdapter<NewsEntry> {
    public NewsArrayAdapter(Context context, ArrayList<NewsEntry> news) {
        super(context, 0, news);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        NewsEntry news = getItem(position);
        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.news_item, parent, false);
        }

        TextView date = (TextView)convertView.findViewById(R.id.news_date);
        ImageView image = (ImageView)convertView.findViewById(R.id.news_image);
        TextView synopsis = (TextView)convertView.findViewById(R.id.news_synopsis);

        date.setText(String.format("%d.%d", news.date.getDate(), news.date.getMonth()));
        image.setImageBitmap(news.bitmap);

        synopsis.setText(news.synopsis);

        return convertView;
    }
}
