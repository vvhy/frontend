package com.example.bfu_helper_10;

import java.util.List;

public class DayOfSchedule {
    private String dayOfWeek, month;
    private int date;
    private List<Subject> subjects;

    public DayOfSchedule(String dayOfWeek, String month, int date, List<Subject> subjects) {
        this.dayOfWeek = dayOfWeek;
        this.month = month;
        this.date = date;
        this.subjects = subjects;
    }

    public String getDayOfWeek() {
        return dayOfWeek;
    }

    public String getMonth() {
        return month;
    }

    public String getDate() {
        return Integer.toString(date);
    }

    public List<Subject> getSubjects() {
        return subjects;
    }

    public void setDayOfWeek(String dayOfWeek) {
        this.dayOfWeek = dayOfWeek;
    }

    public void setMonth(String month) {
        this.month = month;
    }

    public void setDate(int date) {
        this.date = date;
    }

    public void setSubjects(List<Subject> subjects) {
        this.subjects = subjects;
    }
}
