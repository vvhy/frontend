package com.example.bfu_helper_10;

public class Subject {
    private String time, subject;
    private int image;

    public Subject(String time, String subject, int image) {
        this.time = time;
        this.subject = subject;
        this.image = image;
    }

    public String getTime() {
        return time;
    }

    public String getSubject() {
        return subject;
    }

    public int getImage() {
        return image;
    }
}
